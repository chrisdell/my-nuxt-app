# my-nuxt-app

> My superior Nuxt.js project

## Build Setup

``` bash
# install dependencies
$ yarn install

# serve with hot reload at localhost:3000
$ yarn run dev

# build for production and launch server
$ yarn run build
$ yarn start

# generate static project
$ yarn run generate
```

Generated static files are stored in `./dist` directory.

For detailed explanation on how things work, checkout [Nuxt.js docs](https://nuxtjs.org).
